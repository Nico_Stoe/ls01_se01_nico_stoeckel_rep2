import java.util.Scanner;

public class Quersumme {
    
	public static void main(String [] args) {
        
		Scanner newScanner = new Scanner(System.in);
		
		System.out.println("Geben Sie die Zahl f�r die Quersumme ein");
		
		int zahl = newScanner.nextInt();  
        int quer, i, rest;
        quer = 0;
        i = zahl;
        while (zahl > 0) {
            rest = zahl % 10;
            quer = quer + rest;
            zahl = zahl / 10;
        }
        System.out.println("Quersumme von " + i + " = "  + quer ) ;

    }

}