import java.util.Scanner;

class FahrkartenautomatMitArray {
    public static void main(String[] args) {
        @SuppressWarnings("resource")
		Scanner tastatur = new Scanner(System.in);

        int anzahlTickets;
        double zuZahlenderBetrag;
        double ticketpreis = 0;
        double eingezahlterGesamtbetrag;
        double eingeworfeneMuenze;
        double rueckgabebetrag;
        //double eingegebenerBetrag;

        //Ticketauswahl
    	System.out.println(" Bitte w�hlen Sie ein Ticket");
		System.out.println(" 1) Einzelfahrschein Berlin AB         [2,90 EUR] ");
		System.out.println(" 2) Einzelfahrschein Berlin BC         [3,30 EUR] ");
		System.out.println(" 3) Einzelfahrschein Berlin ABC        [3,60 EUR] ");
		System.out.println(" 4) Kurzstrecke                        [1,90 EUR] ");
		System.out.println(" 5) Tageskarte Berlin AB               [8,60 EUR] ");
		System.out.println(" 6) Tageskarte Berlin BC               [9,00 EUR] ");
		System.out.println(" 7) Tageskarte Berlin ABC 	       [9,60 EUR] ");
		System.out.println(" 8) Kleingruppen-Tageskarte Berlin AB  [23,50 EUR]");
		System.out.println(" 9) Kleingruppen-Tageskarte Berlin BC  [24,30 EUR]");
		System.out.println("10) Kleingruppen-Tageskarte Berlin ABC [24,90 EUR]");
		System.out.println("11) Ende");
		
		System.out.print("\nIhre Auswahl:");
		
		/*double [] t = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
		
		for(int i=0; i<t.length; i++)
			System.out.println(t[i]); */
		
		int auswahl = tastatur.nextInt();
		
		if (auswahl == 1) {
			System.out.println("Sie haben den Einzelfahrschein Berlin AB gew�hlt");
			ticketpreis = 2.90;
			}
		if (auswahl == 2) {
			System.out.println("Sie haben den Einzelfahrschein Berlin BC gew�hlt");
			ticketpreis = 3.30;
			}
		if (auswahl == 3) {
			System.out.println("Sie haben den Einzelfahrschein Berlin ABC gew�hlt");
			ticketpreis = 3.60;
			
		if (auswahl == 4) {
			System.out.println("Sie haben das Kurzstrecken Ticket gew�hlt");
			ticketpreis = 1.90;
			}
		if (auswahl == 5) {
			System.out.println("Sie haben die Tageskarte Berlin AB gew�hlt");
			ticketpreis = 8.60;
			}
		if (auswahl == 6) {
			System.out.println("Sie haben die Tageskarte Berlin BC gew�hlt");
			ticketpreis = 9.00;
			
		if (auswahl == 7) {
			System.out.println("Sie haben die Tageskarte Berlin ABC gew�hlt");
			ticketpreis = 9.60;
			}
		if (auswahl == 8) {
			System.out.println("Sie haben das Kleingruppen Ticket AB gew�hlt");
			ticketpreis = 23.50;
			}
		if (auswahl == 9) {
			System.out.println("Sie haben das Kleingruppen-Tageskarte Berlin BC gew�hlt");
			ticketpreis = 24.30;
			}
		if (auswahl == 10) {
			System.out.println("Sie haben das Kleingruppen-Tageskarte Berlin ABC gew�hlt");
		    ticketpreis = 24.90;
		    }
		if (auswahl == 11) {
			System.out.println("Ihr Vorgang wird abgebrochen ");
			for (int i = 0; i < 10; i++) {
	            System.out.print("=");
	            try {
	                Thread.sleep(250);
	            } catch (InterruptedException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }
	      
	        }
		}	
			
			 System.out.println("\n");
			
			System.out.println("M�chten Sie das Programm neustarten? j/n? ");
			String restart = tastatur.next();
			if(restart.equals("j"))
				main(args);
			else{
				System.out.println("Das Programm wird beendet ");
				for (int i = 0; i < 10; i++) {
		            System.out.print("=");
		            try {
		                Thread.sleep(250);
		            } catch (InterruptedException e) {
		                // TODO Auto-generated catch block
		                e.printStackTrace();
		            }
		        }
				System.out.println("\n");
				System.out.println("Das Programm wurde beendet");
				System.exit(0);
			}
		}
		if (auswahl >= 12) {
			System.out.println("Fehler! Kehre zum Men� zur�ck ");
			for (int i = 0; i < 10; i++) {
	            System.out.print("=");
	            try {
	                Thread.sleep(250);
	            } catch (InterruptedException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }
	        }
	        System.out.println("\n\n");
			main(args);
		}
			
		System.out.println("Ticketpreis (EURO-Cent): " + ticketpreis +"0" + "�");

        System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextInt();

        zuZahlenderBetrag = ticketpreis * anzahlTickets;

        // Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0;
        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.format("Noch zu zahlen: %4.2f �%n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct): ");
            eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }

        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 10; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");

        // R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        
        if (rueckgabebetrag > 0.0) {
        	System.out.format("Der R�ckgabebetrag in H�he von %4.2f � %n", rueckgabebetrag);
            System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("\n");
        
        System.out.println("M�chten Sie Ihren Einkauf fortsetzen? j/n? ");
		String restart = tastatur.next();
		
		if(restart.equals("j"))
			main(args);
		else{
			System.out.println("\nVielen Dank f�r Ihren Einkauf!" + "\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
	                + "Wir w�nschen Ihnen eine gute Fahrt.");
			System.exit(0);
		}
          
    }
 
}
}

