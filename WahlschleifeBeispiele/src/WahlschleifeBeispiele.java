import java.util.Scanner;

public class WahlschleifeBeispiele {

	public static void main(String[] args) {
	
		Scanner newScanner = new Scanner(System.in);
	
		System.out.println("Bis welche Zahl soll ausgegeben werden?");
		
		int bisZahl = newScanner.nextInt();
		int n = 1;
		
		while(n < bisZahl) {
			System.out.println(n);
			n++;
		}
		
		
	}

}
