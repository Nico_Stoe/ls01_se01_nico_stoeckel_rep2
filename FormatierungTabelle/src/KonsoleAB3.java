
public class KonsoleAB3 {

	public static void main(String[] args) {
		
		System.out.printf("%-12s  %s %10s  \n" , "Fahrenheit","|","Celsius");
		System.out.printf("%s              \n" , "-------------------------");
		System.out.printf("%-12d  %s %9.2f \n" , -20         ,"|",   -28.89);
		System.out.printf("%-12d  %s %9.2f \n" , -10         ,"|",   -23.33);
		System.out.printf("%+-13d %s %9.2f \n" ,   0         ,"|",   -17.78);
		System.out.printf("%+-13d %s %9.2f \n" ,  20         ,"|",    -6.67);
		System.out.printf("%+-13d %s %9.2f \n" ,  20         ,"|",    -1.11);		
	}

}
