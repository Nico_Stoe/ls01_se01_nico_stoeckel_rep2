import java.util.*;

public class Rabattsystem {

	public static void main(String[] args) {
	
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Geben Sie den Bestellwert ein: ");
		float bestellwert = scan.nextFloat();
		
		if(bestellwert >=0 && bestellwert <= 100) {
		System.out.println("Gl�ckwunsch! Sie bekommen 10% Rabatt");
		double rabatt1 = bestellwert - (bestellwert * 0.19);
		double rabatt10 = rabatt1 - (rabatt1 * 0.10);
		System.out.println("Mit dem Rabatt betr�gt der Preis: " + rabatt10);	
		}
		
		if(bestellwert >100 && bestellwert <=500) {
		System.out.println("Gl�ckwunsch! Sie bekommen 15% Rabatt");
		double rabatt2 = bestellwert - (bestellwert * 0.19);
		double rabatt15 = rabatt2 - (rabatt2 * 0.15);
		System.out.println("Mit dem Rabatt betr�gt der Preis: " + rabatt15);
		}
		
		if(bestellwert >500) {
		System.out.println("Gl�ckwunsch! Sie bekommen 20% Rabatt");
		double rabatt3 = bestellwert - (bestellwert * 0.19);
		double rabatt20 = rabatt3 - (rabatt3 * 0.2);
		System.out.println("Mit dem Rabatt betr�gt der Preis: " + rabatt20);
		}
	}
}
