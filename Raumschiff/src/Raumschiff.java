import java.util.ArrayList;

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static String broadcastKommunikator;
	private Ladung ladungsverzeichnis;
	
	public Raumschiff() {
		super();
	}
	
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		super();
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}
	
	ArrayList<String> y = new ArrayList<String>();
	ArrayList<Ladung> z = new ArrayList<Ladung>();
	
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	public String getSchiffsname() {
		return schiffsname;
	}
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	public void photonentorpedoSchiessen(Raumschiff r) {
		if (photonentorpedoAnzahl <= 0) {
			System.out.println("-=*Click*=-");
		} else {
				photonentorpedoAnzahl--;
		System.out.println("Photonentorpedo abgeschossen");
		r.treffer(r);
		//nachrichtenAnAlle();//
		}
	}
		
	public void phaserkanoneSchiessen(Raumschiff r) {
		if (energieversorgungInProzent < 50) {
			System.out.println("-=*Click*=-");
		} else {
			energieversorgungInProzent = energieversorgungInProzent - 50;
		System.out.println("Phaserkanone abgeschossen");
		r.treffer(r);
		//nachrichtenAnAlle();//
		}
	}
	
	private void treffer(Raumschiff r) {
		System.out.println(r.getSchiffsname() + " wurde getroffen");
	}

	public void nachrichtenAnAlle(String message) {
		
	}
	
	public void eintraegeLogbuchZurueckgeben() {
		ArrayList<String> y = new ArrayList<String>();
	}
	
	public void photonentorpedosLaden(int anzahlTorpedos) {
		
	}
	
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
		
	}
	
	public void addLadung(Ladung neueLadung) {
		neueLadung.setBezeichnung("Baumwolle");
		neueLadung.setMenge(90);
		
		System.out.println(neueLadung.getBezeichnung());
		System.out.println(neueLadung.getMenge());
	}
	
	public void zustandRaumschiff(Raumschiff r) {
		r.setAndroidenAnzahl(3);
		r.setEnergieversorgungInProzent(80);
		r.setHuelleInProzent(70);
		r.setLebenserhaltungssystemeInProzent(90);
		r.setPhotonentorpedoAnzahl(5);
		r.setSchiffsname("Andromeda");
		r.setSchildeInProzent(50);
		
		System.out.println(r.getAndroidenAnzahl());
		System.out.println(r.getEnergieversorgungInProzent());
		System.out.println(r.getHuelleInProzent());
		System.out.println(r.getLebenserhaltungssystemeInProzent());
		System.out.println(r.getPhotonentorpedoAnzahl());
		System.out.println(r.getSchiffsname());
		System.out.println(r.getSchildeInProzent()); 
		
	}

	public void ladungsverzeichnisAusgeben() {
		
	}
	
	public void ladungsverzeichnisAufraeumen() {
		
	}

	public static void main (String [] args) {
		
		Raumschiff r = new Raumschiff();
		Ladung neueLadung = new Ladung();
		
		r.zustandRaumschiff(r);
		r.phaserkanoneSchiessen(r);
		r.photonentorpedoSchiessen(r); 
		
		neueLadung.addLadung(neueLadung);
	
	}
}

