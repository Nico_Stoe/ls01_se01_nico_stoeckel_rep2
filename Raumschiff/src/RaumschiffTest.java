
public class RaumschiffTest {

	public static void main (String [] args) {
		
		Raumschiff klingonen = new Raumschiff();
		Raumschiff romulaner = new Raumschiff();
		Raumschiff vulkanier = new Raumschiff();
		
	    Ladung klingonenLadung = new Ladung();
		Ladung romulanerLadung = new Ladung();
		Ladung vulkanierLadung = new Ladung();
				
		
		klingonen.photonentorpedoSchiessen(romulaner);			
		romulaner.phaserkanoneSchiessen(klingonen);
		System.out.println("Vulkanier: Gewalt ist nicht logisch");
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		
		System.out.println("Zustand Klingonen: ");
		klingonen.setPhotonentorpedoAnzahl(1);
		klingonen.setEnergieversorgungInProzent(100);
		klingonen.setSchildeInProzent(100);
		klingonen.setHuelleInProzent(100);
		klingonen.setLebenserhaltungssystemeInProzent(100);
		klingonen.setSchiffsname("IKS Hegh'ta");
		klingonen.setAndroidenAnzahl(2);
		
		System.out.println(klingonen.getPhotonentorpedoAnzahl());
		System.out.println(klingonen.getEnergieversorgungInProzent());
		System.out.println(klingonen.getSchildeInProzent()); 
		System.out.println(klingonen.getHuelleInProzent());
		System.out.println(klingonen.getLebenserhaltungssystemeInProzent());
		System.out.println(klingonen.getSchiffsname());
		System.out.println(klingonen.getAndroidenAnzahl());

		System.out.println("Ladung Klingonen: ");
		klingonenLadung.setBezeichnung("Ferengi Schneckensaft");
		klingonenLadung.setMenge(200);
		System.out.println(klingonenLadung.getBezeichnung());
		System.out.println(klingonenLadung.getMenge());
		klingonenLadung.setBezeichnung("Bat'leth Klingonen Schwert");
		klingonenLadung.setMenge(200);
		System.out.println(klingonenLadung.getBezeichnung());
		System.out.println(klingonenLadung.getMenge());
	
		System.out.println("Zustand Romulaner: ");
		romulaner.setPhotonentorpedoAnzahl(2);
		romulaner.setEnergieversorgungInProzent(100);
		romulaner.setSchildeInProzent(100);
		romulaner.setHuelleInProzent(100);
		romulaner.setLebenserhaltungssystemeInProzent(100);
		romulaner.setSchiffsname("IRW Khazara");
		romulaner.setAndroidenAnzahl(2);
		
		System.out.println(romulaner.getPhotonentorpedoAnzahl());
		System.out.println(romulaner.getEnergieversorgungInProzent());
		System.out.println(romulaner.getSchildeInProzent()); 
		System.out.println(romulaner.getHuelleInProzent());
		System.out.println(romulaner.getLebenserhaltungssystemeInProzent());
		System.out.println(romulaner.getSchiffsname());
		System.out.println(romulaner.getAndroidenAnzahl());

		System.out.println("Ladung Romulaner: ");
		romulanerLadung.setBezeichnung("Borg-Schrott");
		romulanerLadung.setMenge(5);
		System.out.println(romulanerLadung.getBezeichnung());
		System.out.println(romulanerLadung.getMenge());
		romulanerLadung.setBezeichnung("Rote Materie");
		romulanerLadung.setMenge(2);
		System.out.println(romulanerLadung.getBezeichnung());
		System.out.println(romulanerLadung.getMenge());
		romulanerLadung.setBezeichnung("Plasma-Waffe");
		romulanerLadung.setMenge(50);
		System.out.println(romulanerLadung.getBezeichnung());
		System.out.println(romulanerLadung.getMenge());
	
		System.out.println("Zustand Vulkanier: ");
		vulkanier.setPhotonentorpedoAnzahl(0);
		vulkanier.setEnergieversorgungInProzent(80);
		vulkanier.setSchildeInProzent(80);
		vulkanier.setHuelleInProzent(50);
		vulkanier.setLebenserhaltungssystemeInProzent(100);
		vulkanier.setSchiffsname("NI'Var");
		vulkanier.setAndroidenAnzahl(5);
		
		System.out.println(vulkanier.getPhotonentorpedoAnzahl());
		System.out.println(vulkanier.getEnergieversorgungInProzent());
		System.out.println(vulkanier.getSchildeInProzent()); 
		System.out.println(vulkanier.getHuelleInProzent());
		System.out.println(vulkanier.getLebenserhaltungssystemeInProzent());
		System.out.println(vulkanier.getSchiffsname());
		System.out.println(vulkanier.getAndroidenAnzahl());

		System.out.println("Ladung Vulkanier: ");
		vulkanierLadung.setBezeichnung("Forschugssonde");
		vulkanierLadung.setMenge(35);
		System.out.println(vulkanierLadung.getBezeichnung());
		System.out.println(vulkanierLadung.getMenge());
		vulkanierLadung.setBezeichnung("Photonentorpedo");
		vulkanierLadung.setMenge(3);
		System.out.println(vulkanierLadung.getBezeichnung());
		System.out.println(vulkanierLadung.getMenge());

				
		
	
	}

}
