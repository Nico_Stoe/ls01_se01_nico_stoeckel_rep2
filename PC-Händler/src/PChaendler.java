import java.util.Scanner;

public class PChaendler {

  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);
   
    String artikel = liesString("Geben Sie den Artikel ein: ");
    
    int anzahl = liesInt("Geben Sie die Anzahl ein: ");
    
    
    double preis = liesDouble("Geben Sie den Nettopreis ein:");
    

    double mwst  = liesDouble2("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
    

    berechneGesamtnettopreis(anzahl, preis);
    
    berechneGesamtbruttopreis(mwst, preis);
    
    rechnungausgeben(artikel,anzahl,preis,preis,mwst);
    
  }
  
  
  public static String liesString(String text) {
	  Scanner myScanner = new Scanner(System.in);
	  System.out.println(text);
	  String str = myScanner.next();
	  return str;
  	}	
  
  public static int liesInt(String text) {
	  Scanner myScanner  = new Scanner(System.in);
	  System.out.println(text);
	  int erg = myScanner.nextInt();
	  return erg;
  	}
  
  public static double liesDouble(String text) {
	  Scanner myScanner  = new Scanner(System.in);
	  System.out.println(text);
	  int dob = myScanner.nextInt();
	  return dob;
  	} 
  
  public static double liesDouble2(String text) {
	  Scanner myScanner  = new Scanner(System.in);
	  System.out.println(text);
	  int dob2 = myScanner.nextInt();
	  return dob2;
  	}
  
  public static void berechneGesamtnettopreis(double preis, double anzahl) {
	  System.out.println("Der Netto Preis betr�gt: " + anzahl * preis);
	 
  	}
  
  public static void berechneGesamtbruttopreis(double nettogesamtpreis,double mwst) {
  		System.out.println("Der Brutto Preis betr�gt: " + (nettogesamtpreis * (1 + mwst / 100));  		
  } 

  public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,double mwst) {
	  System.out.println("\tRechnung");
	  System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
	  System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
 
  }
  
}
