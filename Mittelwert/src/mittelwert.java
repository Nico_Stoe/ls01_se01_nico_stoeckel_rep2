public class mittelwert {

   public static void main(String[] args) {

	  double x = 2.0;
      double y = 4.0;
      double m;
      
     m = berechneMittelwert(x,y);
     
     printMittelwert(x,y,m); 
    
   }
   
   public static void printMittelwert(double zahl1, double zahl2, double Mittelwert) {
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", zahl1, zahl2, Mittelwert);
   }
   
   public static double berechneMittelwert(double zahl1,double zahl2) {
	   double erg = (zahl1 + zahl2) / 2.0;
	   return erg;
   }

}
